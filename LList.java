public class LList<X>
{
    Node<X> head;
    Node<X> last;
    Node <X>  value;
    int size =0;
    //Node<X> size;
    public LList()
    {
        head = null;
    }
    
   public void addLast(X s)
    {
        if (head == null)
        {
            head = new Node<X>(s, null);
        }
        else
        {
            //Node n = head;  // Start at the head
            //while (n.next != null)  // As long as there is another Node
            //{
                // Hop to the next one
            //    n = n.next;
            //}
            Node<X> n;
            for (n = head; n.next != null; n = n.next);
            
            // Now we are at the last Node
            n.next = new Node<X>(s, null);
        }
     }
    
    public void addFirst(X s)
    {
        head = new Node<X>(s, head);
    }
    
    
    public void add(X s) 
    {
         if (head == null)
        {
            head = new Node<X>(s,null);
        }
        else
        {
            Node<X> n = head;  // Start at the head
            while (n.next != null)  // As long as there is another Node
            {
                // Hop to the next one
               n = n.next;
            }
           // Node<X> n;
            //for (n = head; n.next != null; n = n.next)
            //n = n.next;
           
            // Now we are at the last Node
            n.next = new Node<X>(s,null);
        }
        
     }        
//Same as addLast.
       
     public void removeFirst() {
         
        if(size()>0) 
        {
        head = head.next;
        size--;
    }
    
    else
       throw new Inception();
          
    
}

 
//Remove the head of the list. Throws an Inception exception if the array is already empty.
       
    public void removeLast()
      {  
         if(size()==1) clear();
         else if(size()>0)
       {
     Node n;
     for(n=head; n.next.next !=null; n=n.next);
     n.next=null;
     last =n;
     size--;
     
    }
    else throw new Inception();
   }

//Delete the node at the given index. Throw an Inception if the index is invalid.
    
    
    
    
  
    // Do this
    public int size() 
    {
        //head=last=null;
        //return 0;
        int nodenumber=1;
       Node <X> n;
       if(head==null)
       {
           
           
           return 0;
    }
    for(n = head; n.next!=null; n=n.next) {
        
       nodenumber +=1; 
        
    }
    
    return nodenumber;
   }
    // Do this
    public X get(int index)
    {
     
            
    if (index < 0 || index >= size()) 
          
      throw new Inception();
        
      
			Node<X> n=head;
		for (int i = 0; i < index; i++) { 
			
       n = n.next;
   
   }	
   return n.data;
   }
               

        
        
    
   

    
    // Do this
    public int indexOf(X target)
    {   
       
        int index =0;
      
     
          Node<X> n=head;
          
          
            for (int i=0; i<size()+1; i++)  
            {  
                
               if(n.data.equals(target))
                   
               
                
                return index;
           
            
            }
            
 index++;
          //index++;
      return -1;
     
    }
   
   
    // Needs work
    public void set(int index, X s)
    {
      
          //if (index < 0 && index >= size()-1)   throw new Inception();
          // {
              
               if (index < 0 || index >= size()) 
          
      throw new Inception();
            
      
       
        Node<X> n = head;
        
         
        
        // Start at the head
        for (int i = 0; i < index; i++)  // Count as we go
        
           {  
           n = n.next;  // Hop to the next Node
          //head = new Node<X>(s, head);
           }
            n.data = s;
            
        
          
            
         
         
          
       
        }
            
           
    public void remove(int index)
    {
        //Node n;
      //if(n = index) {
           
         if(size()==1) clear();
         else if(size()>0)
       {
     Node n;
     for(n=head; n.next.next !=null; n=n.next);
     n.next=null;
     last =n;
     size++;
     
    }
    else throw new Inception();
    

     
          
     
      
     
        
}
    
    
  
	
    // Do this
    public void clear()
    {
        head =null;
        last =null;
        size=0;
        //Node<X> n;
        //for (n = head; n.next != null; n = n.next)
       // Node n=n.next;
    // n.data = null;   
    }
    
    public void insert(int index, X s)
    {
         if(index ==0) addFirst(s);
         else if (index > 0 && index < size()) 
         {
             Node<X> n=head;
           for(int i=0; i<index-1; i++)  n=n.next; 
           
           n.next= new Node<X>(s,n.next);
           size++;
       
        
        }
        else
         throw new Inception();
    //
   
    }
    
   // Do this
    public String toString()
    {   
        if(size()>0)
        {
            
            String mass ="["+get(0);
            Node<X> n=head;
            for(int i=1; i<size(); i++)
            {
                n=n.next;
                mass+=", "+n.data;
                
            }
       return mass+"]"; 
      
  }
  else return "[]";
}
}